import java.time.LocalDate;

public class Book extends LibItem {
    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    String genre;

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    LocalDate dueDate;

    public int getNumPages() {
        return numPages;
    }

    public void setNumPages(int numPages) {
        this.numPages = numPages;
    }

    int numPages;

    Book(String title, String genre) {
        this.title = title;
        this.genre = genre;
    }

    @Override
    Boolean isBorrowable() {
        return true;
    }

}
