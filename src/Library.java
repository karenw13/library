import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

public class Library {

    public enum Category{
        BOOKS, CDS, DVDS, PERIODICALS
    }
    HashMap<Category, ArrayList<LibItem>> libraryItems = new HashMap();

    static int idCount = 0;

    Library() {

        libraryItems.put(Category.BOOKS, new ArrayList<LibItem>());
        libraryItems.put(Category.CDS, new ArrayList<LibItem>());
        libraryItems.put(Category.DVDS, new ArrayList<LibItem>());
        libraryItems.put(Category.PERIODICALS, new ArrayList<LibItem>());

    }

    void addItem(Category category, LibItem item) {
        libraryItems.get(category).add(item);
        System.out.println("Successfully added "+item.title+", id #: "+item.id+" to "+category+"");
    }

    void removeItem(Category category, LibItem item) {
        if (libraryItems.get(category).contains(item)) {
            libraryItems.get(category).remove(item);
            System.out.println("Successfully removed item");
        }
    }

    void borrowItem(Category category, LibItem item) {
        if (!item.isBorrowable()) {
            System.out.println("Sorry, you cannot borrow this item.");
            return;
        }
        if (libraryItems.get(category).contains(item) && !item.checkedOut) {
            item.setCheckedOut(true);
            LocalDate dueDate = LocalDate.now().plusDays(14); //Due date is 2 weeks from checkout
            item.setDueDate(dueDate);
            System.out.println("You're all set! Due back on "+dueDate.toString()+" Enjoy!");
        }
    }

    void returnItem(Category category, LibItem item) {
        if (!item.checkedOut) {
            System.out.println("You cannot return a book that was not checked out!\n");
            return;
        }
//        if (isOverdue(item)) {
//            // add late fees
//
//        }
        item.setCheckedOut(false);
    }

    Boolean isOverdue(LibItem item) {
        if (LocalDate.now().isAfter(item.getDueDate())) {
            System.out.println("This item is overdue!");
            return true;
        }
        return false;
    }
    void seeTitles(Category item) {
        System.out.println("Available titles for " +item.toString()+": " );
        for (LibItem item1 : libraryItems.get(item)) {
            if (!item1.checkedOut)
                System.out.println("\t" + item1.title);
        }

    }

    void seeAllTitles() {
        System.out.println("Available titles: " );
        for (Category cat : Category.values()) {
            System.out.println(cat.toString());
            for (LibItem item : libraryItems.get(cat)) {
                if (!item.checkedOut)
                    System.out.println("\t" + item.title);
            }
        }
    }
}
