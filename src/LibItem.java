import java.time.LocalDate;

public abstract class LibItem {
    String title;
    int id;

    public Boolean getCheckedOut() {
        return checkedOut;
    }

    public void setCheckedOut(Boolean checkedOut) {
        this.checkedOut = checkedOut;
    }

    Boolean checkedOut = false;

    LibItem(String title) {
        this.title = title;

    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    LocalDate dueDate;

    LibItem() {
        id = Library.idCount;
        Library.idCount++;
    }

    public int getId() {
        return id;
    }

    abstract Boolean isBorrowable();

}
