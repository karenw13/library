
public class UseLibrary {

    public static void main(String[] args) {
        Library lib = new Library();

        // Create Library Items
        Book book1 = new Book("Cinderella", "Fantasy");
        Book book2 = new Book("Harry Potter and the Prisoner of Azkaban", "Fantasy");
        Book book3 = new Book("Algorithms", "Non-Fiction");

        DVD dvd1 = new DVD("Pirates of the Caribbean 1");
        DVD dvd2 = new DVD("Pirates of the Caribbean 2");
        DVD dvd3 = new DVD("Pirates of the Caribbean 3");

        CD cd1 = new CD("song");
        Periodical pd1 = new Periodical("The Washington Post");

        lib.addItem(Library.Category.BOOKS, book1);
        lib.addItem(Library.Category.BOOKS, book2);
        lib.addItem(Library.Category.BOOKS, book3);
        lib.addItem(Library.Category.DVDS, dvd1);
        lib.addItem(Library.Category.DVDS, dvd2);
        lib.addItem(Library.Category.DVDS, dvd3);
        lib.addItem(Library.Category.CDS, cd1);
        lib.addItem(Library.Category.PERIODICALS, pd1);
        System.out.println("----------------------Finished Creating Items------------------------");

        lib.seeTitles(Library.Category.BOOKS);
        System.out.println("---------------------------------------------");
        lib.removeItem(Library.Category.BOOKS, book1);
        System.out.println("----------------------Removed Cinderella, Should Not Appear Below------------------------");
        lib.seeTitles(Library.Category.BOOKS);

        System.out.println("Checking out items------------------------");
        lib.borrowItem(Library.Category.BOOKS, book2);
        lib.borrowItem(Library.Category.CDS, cd1);
        System.out.println("Checking out a Periodical should fail------------------------");
        lib.borrowItem(Library.Category.PERIODICALS, pd1); //This should cause an error message
        System.out.println("----------------Harry Potter and song should no longer appear---------------------");
        lib.seeAllTitles();

        lib.returnItem(Library.Category.CDS, cd1);
        System.out.println("---------------------song should be back------------------------");

        lib.seeAllTitles();
    }
}