public class Periodical extends LibItem {

    Periodical(String title) {
        this.title = title;
    }
    @Override
    Boolean isBorrowable() {
        return false;
    }

    enum type{
        newspaper, magazine, scholarly_journal, newsletter
    }

    type pType;
    int numPages;

    public Periodical(String title, type pt) {
        this.title = title;
        this.pType = pt;
    }

    public type getpType() {
        return pType;
    }

    public void setpType(type pType) {
        this.pType = pType;
    }

    public int getNumPages() {
        return numPages;
    }

    public void setNumPages(int numPages) {
        this.numPages = numPages;
    }


}
